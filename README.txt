Drupal uc_mpower module:
------------------------
Maintainers:
  Sadart Abukari (https://drupal.org/user/1031748)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------



Features:
---------



Installation:
------------
1. Download and unpack the Libraries module directory in your modules folder
   (this will usually be "sites/all/modules/").
   Link: http://drupal.org/project/libraries
2. Download and unpack the uc_mpower module directory in your modules folder
   (this will usually be "sites/all/modules/").
3. Download and unpack the Mpower Library to  "sites/all/libraries".
    Make sure the path to the mpower.php file resides in :
    "sites/all/libraries/mpower_php-master/mpower.php"
   Link: https://github.com/nukturnal/mpower_php/archive/master.zip

4. Go to "Administer" -> "Modules" and enable the Mpower Payments module.


Configuration:
-------------



TODO
// Enforce that mpower library is found before savingg this payment method for it to be active.
